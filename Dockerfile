FROM ubuntu:latest

ARG hugo_version=0.72.0

RUN apt-get update \
    && apt-get install -y cadaver wget \
    && wget https://github.com/gohugoio/hugo/releases/download/v${hugo_version}/hugo_${hugo_version}_Linux-64bit.deb \
    && dpkg -i hugo_${hugo_version}_Linux-64bit.deb \
    && rm -rf /var/lib/apt/lists/*

# https://gist.github.com/alkrauss48/2dd9f9d84ed6ebff9240ccfa49a80662
RUN mkdir -p /home/hudaver

RUN groupadd -r hudaver \
    && useradd -r -g hudaver -d /home/hudaver -s /sbin/nologin -c "main user" hudaver

ENV HOME=/home/hudaver
RUN chown -R hudaver:hudaver $HOME

USER hudaver
